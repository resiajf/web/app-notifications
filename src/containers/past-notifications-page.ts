import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { clearError } from 'src/common/redux/errors/actions';
import { NotificationsPage } from 'src/components/pages/notifications-page';
import {
    NotificationsPageDispatchToProps,
    NotificationsPageStateToProps
} from 'src/components/pages/notifications-page.interfaces';
import { State } from 'src/redux';
import { clearPages, discard, loadPage, markAsRead, unsetLoading } from 'src/redux/notifications/actions';

const mapStateToProps = ({ errors, notifications }: State): NotificationsPageStateToProps => ({
    coisas: notifications.notifications,
    loading: notifications.loading,
    notFoundError: errors.notFound,
    notifications: notifications.notifications[notifications.page || 0],
    serverError: errors.serverUnavailable,
    totalPages: notifications.totalPages,
});

const mapDispatchToProps = (dispatch: any): NotificationsPageDispatchToProps => ({
    clearError: (what) => dispatch(clearError(what)),
    clearLoading: () => dispatch(unsetLoading()),
    clearPages: () => dispatch(clearPages()),
    discard: (notification) => dispatch(discard(notification)),
    loadPage: (page, shitt) => dispatch(loadPage(shitt, page, false)),
    markAsRead: (notification) => dispatch(markAsRead(notification)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(NotificationsPage));