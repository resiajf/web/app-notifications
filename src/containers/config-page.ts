import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { clearError } from 'src/common/redux/errors/actions';
import { ConfigPage } from 'src/components/pages/config-page';
import { ConfigPageDispatchToProps, ConfigPageStateToProps } from 'src/components/pages/config-page.interfaces';
import { State } from 'src/redux';
import {
    changeEnabledStatusForEmail,
    changeEnabledStatusForTelegram,
    checkActivationStatusForTelegram, clearCheckingForTelegram, clearUpdating, loadEmailConfig, loadTelegramConfig
} from 'src/redux/config/actions';

const mapStateToProps = ({ config, errors, user }: State): ConfigPageStateToProps => ({
    ...config,
    serverError: errors.serverUnavailable,
    user,
});

const mapDispatchToProps = (dispatch: any): ConfigPageDispatchToProps => ({
    changeEnabledStatusForEmail: (a) => dispatch(changeEnabledStatusForEmail(a)),
    changeEnabledStatusForTelegram: (a, b) => dispatch(changeEnabledStatusForTelegram(a, b)),
    checkActivationStatusForTelegram: () => dispatch(checkActivationStatusForTelegram()),
    clearCheckingForTelegram: () => dispatch(clearCheckingForTelegram()),
    clearError: (what) => dispatch(clearError(what)),
    clearUpdating: () => dispatch(clearUpdating()),
    loadEmailConfig: () => dispatch(loadEmailConfig()),
    loadTelegramConfig: () => dispatch(loadTelegramConfig()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ConfigPage));