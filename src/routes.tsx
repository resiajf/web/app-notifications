import React from 'react';
import { Redirect } from 'react-router';

import asyncComponent from 'src/common/components/async-component';
import { Route, route } from 'src/common/lib/utils';

export const moduleName = 'notifications';

const NotificationsPage = asyncComponent(() => import('./containers/notifications-page'));
const PastNotificationsPage = asyncComponent(() => import('./containers/past-notifications-page'));
const ManageConfigPage = asyncComponent(() => import('./containers/config-page'));

export const routes: Array<Route<any, any>> = [
    route('/', 'home', () => <Redirect to="/1" />, 'home', { exact: true }),
    route('/:page(\\d+)', 'home', NotificationsPage, 'realHome', { hide: true }),
    route('/past', 'past', () => <Redirect to="/past/1" />, 'past', { exact: true }),
    route('/past/:page(\\d+)', 'past', PastNotificationsPage, 'realPast', { hide: true }),
    route('/manage', 'manage', ManageConfigPage, 'manage', { exact: true }),
];