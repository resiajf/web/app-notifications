import { NotificationConfigBase, NotificationConfigTelegram, NotificationConfigTelegramTest } from 'src/common/lib/api';

export interface NotificationConfig {
    email?: NotificationConfigBase;
    updating: boolean;
    telegram?: NotificationConfigTelegram;
    telegramActivationTest: {
        status?: NotificationConfigTelegramTest,
        loading: boolean,
    };
}