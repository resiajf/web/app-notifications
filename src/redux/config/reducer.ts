import {
    Actions,
    CHECKED_ACTIVATION_CODE,
    CHECKING_ACTIVATION_CODE,
    CLEAR_CHECKING_ACTIVATION_CODE, CLEAR_UPDATING, EMAIL_CONFIG, TELEGRAM_CONFIG, UPDATING
} from 'src/redux/config/actions';
import { NotificationConfig } from 'src/redux/config/state';

export default (state: NotificationConfig, action: Actions) => {
    switch(action.type) {
        case CHECKED_ACTIVATION_CODE: {
            return {
                ...state,
                telegram: action.config ? action.config : state.telegram,
                telegramActivationTest: {
                    loading: false,
                    status: action.status,
                }
            };
        }

        case CHECKING_ACTIVATION_CODE: {
            return {
                ...state,
                telegramActivationTest: {
                    loading: true,
                    status: undefined,
                }
            };
        }

        case CLEAR_CHECKING_ACTIVATION_CODE: {
            return {
                ...state,
                telegramActivationTest: {
                    loading: false,
                    status: undefined,
                }
            };
        }

        case CLEAR_UPDATING: {
            return {
                ...state,
                updating: false,
            };
        }

        case EMAIL_CONFIG: {
            return {
                ...state,
                email: action.config,
                updating: false,
            };
        }

        case TELEGRAM_CONFIG: {
            return {
                ...state,
                telegram: action.config,
                updating: false,
            };
        }

        case UPDATING: {
            return {
                ...state,
                updating: true,
            };
        }

        default: {
            return state || {
                email: undefined,
                telegram: undefined,
                telegramActivationTest: {
                    loading: false,
                    status: undefined,
                },
            };
        }
    }
};