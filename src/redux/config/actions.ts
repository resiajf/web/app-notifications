import { Action, Dispatch } from 'redux';

import {
    NotificationConfigBase,
    NotificationConfigTelegram,
    NotificationConfigTelegramTest,
    Notifications
} from 'src/common/lib/api';
import { catchErrors as f } from 'src/common/redux/errors/decorator';

export const EMAIL_CONFIG = 'config:EMAIL_CONFIG';
export const TELEGRAM_CONFIG = 'config:TELEGRAM_CONFIG';
export const CHECKING_ACTIVATION_CODE = 'config:CHECKING_ACTIVATION_CODE';
export const CHECKED_ACTIVATION_CODE = 'config:CHECKED_ACTIVATION_CODE';
export const CLEAR_CHECKING_ACTIVATION_CODE = 'config:CLEAR_CHECKING_ACTIVATION_CODE';
export const UPDATING = 'config:UPDATING';
export const CLEAR_UPDATING = 'config:CLEAR_UPDATING';

interface EmailConfigAction extends Action<typeof EMAIL_CONFIG> {
    config: NotificationConfigBase;
}

interface TelegramConfigAction extends Action<typeof TELEGRAM_CONFIG> {
    config: NotificationConfigTelegram;
}

type CheckingActivationCodeAction = Action<typeof CHECKING_ACTIVATION_CODE>;

interface CheckedActivationCodeAction extends Action<typeof CHECKED_ACTIVATION_CODE> {
    config?: NotificationConfigTelegram;
    status: NotificationConfigTelegramTest;
}

type ClearCheckingActivationCode = Action<typeof CLEAR_CHECKING_ACTIVATION_CODE>;

type UpdatingAction = Action<typeof UPDATING>;

type ClearUpdating = Action<typeof CLEAR_UPDATING>;

export type Actions = EmailConfigAction | TelegramConfigAction | CheckingActivationCodeAction |
    CheckedActivationCodeAction | ClearCheckingActivationCode | UpdatingAction | ClearUpdating;

export const loadEmailConfig = () => f(async (dispatch: Dispatch) => {
    const config = await Notifications.getConfigEmail();
    dispatch({
        config,
        type: EMAIL_CONFIG,
    });
});

export const loadTelegramConfig = () => f(async (dispatch: Dispatch) => {
    const config = await Notifications.getConfigTelegram();
    dispatch({
        config,
        type: TELEGRAM_CONFIG,
    });
});

export const checkActivationStatusForTelegram = () => f(async (dispatch: Dispatch) => {
    dispatch({ type: CHECKING_ACTIVATION_CODE });
    const status = await Notifications.testEnabledTelegram();
    //If reason is 280, means that the code has expired and so, it has to be regenerated
    if(status.done || (!status.done && status.reason === 280)) {
        const config = await Notifications.getConfigTelegram();
        dispatch({
            config,
            status,
            type: CHECKED_ACTIVATION_CODE,
        });
    } else {
        dispatch({
            status,
            type: CHECKED_ACTIVATION_CODE,
        });
    }
});

export const clearCheckingForTelegram = () => ({
    type: CLEAR_CHECKING_ACTIVATION_CODE,
});

export const changeEnabledStatusForTelegram = (value: boolean, removeChatId?: boolean) => f(async (dispatch: Dispatch) => {
    dispatch({ type: UPDATING });
    const config = await Notifications.modifyConfigTelegram({ enabled: value, chat_id: removeChatId ? null : undefined });
    dispatch({
        config,
        type: TELEGRAM_CONFIG,
    });
});

export const changeEnabledStatusForEmail = (value: boolean) => f(async (dispatch: Dispatch) => {
    dispatch({ type: UPDATING });
    const config = await Notifications.modifyConfigEmail(value);
    dispatch({
        config,
        type: EMAIL_CONFIG,
    });
});

export const clearUpdating = () => ({
    type: CLEAR_UPDATING,
});
