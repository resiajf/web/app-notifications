import { combineReducers } from 'redux';

import { reducers as commonReducers, State as CommonState } from 'src/common/redux';
import config from 'src/redux/config/reducer';
import { NotificationConfig } from 'src/redux/config/state';
import notifications from 'src/redux/notifications/reducer';
import { Notifications } from 'src/redux/notifications/state';

export const reducers = combineReducers<State>({
    ...commonReducers,
    config,
    notifications,
});

// tslint:disable-next-line:no-empty-interface
export interface State extends CommonState {
    config: NotificationConfig;
    notifications: Notifications;
}
