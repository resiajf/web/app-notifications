import { Notification } from 'src/common/lib/api';
import {
    Actions,
    CHANGED_READ_STATUS,
    CLEAR_PAGES,
    DISCARDED,
    LOADED_PAGE,
    LOADING, NEW_NOTIFICATION, SIMPLY_CHANGE_PAGE, UNSET_LOADING
} from 'src/redux/notifications/actions';
import { Notifications } from 'src/redux/notifications/state';

const lookForId = (pages: Notification[][], id: number): [number | null, number | null] => {
    for(let page = 1; page < pages.length; page++) {
        if(pages[page]) {
            const pos = pages[page].findIndex((notification) => notification.id_notificacion === id);
            if(pos !== -1) {
                return [page, pos];
            }
        }
    }
    return [ null, null ];
};

export default (state: Readonly<Notifications>, action: Actions): Notifications => {
    switch(action.type) {
        case LOADING: {
            return {
                ...state,
                loading: true,
                page: action.page || state.page,
            };
        }

        case LOADED_PAGE: {
            const notifications = [ ...state.notifications ];
            notifications[action.page] = action.notifications;
            return {
                ...state,
                itemsPerPage: action.itemsPerPage,
                loading: false,
                notifications,
                page: action.page,
                totalPages: action.totalPages,
            };
        }

        case CLEAR_PAGES: {
            return {
                ...state,
                loading: false,
                notifications: [],
                page: undefined,
                totalPages: undefined,
            };
        }

        case CHANGED_READ_STATUS: {
            const [ page, pos ] = lookForId(state.notifications, action.notificationId);
            if(page !== null) {
                return {
                    ...state,
                    notifications: [
                        ...state.notifications.slice(0, page),
                        [
                            ...state.notifications[page].slice(0, pos!),
                            { ...state.notifications[page][pos!], read: action.read },
                            ...state.notifications[page].slice(pos! + 1),
                        ],
                        ...state.notifications.slice(page + 1),
                    ],
                };
            }
            return state;
        }

        case DISCARDED: {
            const [ page ] = lookForId(state.notifications, action.notificationId);
            if(page !== null) {
                let { totalPages } = state;
                if(page === state.totalPages && state.notifications[page].length === 1) {
                    //If we are in the last page, and it will not have anymore notifications, reduce the total pages
                    totalPages = totalPages! - 1;
                }
                return {
                    ...state,
                    notifications: [
                        ...state.notifications.slice(0, page),
                        //Invalidate the rest of the cache
                    ],
                    totalPages,
                };
            }
            return state;
        }

        case UNSET_LOADING: {
            return {
                ...state,
                loading: false,
            };
        }

        case NEW_NOTIFICATION: {
            const notifications = [];
            let newPage = false;
            //Push the new notification in the pages. The last notification will be discarded :(
            //The pages starts at 1!
            for(let i = state.notifications.length - 1; i > 0; i--) {
                if(state.notifications[i] === undefined) {
                    continue;
                }

                if(i === 1) {
                    if(state.notifications[1].length >= state.itemsPerPage!) {
                        notifications[1] = [
                            action.notification,
                            ...state.notifications[1].slice(0, -1),
                        ];
                    } else {
                        notifications[1] = [
                            action.notification,
                            ...state.notifications[1],
                        ];
                    }
                } else {
                    if(state.notifications[i - 1] === undefined) {
                        continue;
                    }

                    notifications[i] = [
                        state.notifications[i - 1].slice(-1)[0],
                        ...state.notifications[i].slice(0, -1),
                    ];
                }

                if(i === state.totalPages) {
                    if(state.notifications[i].length === state.itemsPerPage) {
                        newPage = true;
                    }
                }
            }

            return {
                ...state,
                loading: !notifications[state.page!] ? true : state.loading,
                notifications,
                totalPages: newPage ? state.totalPages! + 1 : state.totalPages || 1,
            };
        }

        case SIMPLY_CHANGE_PAGE: {
            return {
                ...state,
                page: action.page,
            };
        }

        default: {
            return state || {
                loading: false,
                notifications: [],
            };
        }
    }
};