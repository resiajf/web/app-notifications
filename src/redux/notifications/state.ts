import { Notification } from 'src/common/lib/api';

export interface Notifications {
    itemsPerPage?: number;
    loading: boolean;
    page?: number;
    totalPages?: number;
    notifications: Notification[][];
}