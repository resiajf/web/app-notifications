import { Action, Dispatch } from 'redux';

import { Notification, Notifications } from 'src/common/lib/api';
import { catchErrors as f } from 'src/common/redux/errors/decorator';

export const LOADING = 'notifications:LOADING';
export const LOADED_PAGE = 'notifications:LOADED_PAGE';
export const CLEAR_PAGES = 'notifications:CLEAR_PAGES';
export const CHANGED_READ_STATUS = 'notifications:CHANGED_READ_STATUS';
export const DISCARDED = 'notifications:DISCARDED';
export const UNSET_LOADING = 'notifications:UNSET_LOADING';
export const NEW_NOTIFICATION = 'NEW_NOTIFICATION'; //Unprefixed, let this be like this
export const SIMPLY_CHANGE_PAGE = 'notifications:SIMPLY_CHANGE_PAGE';

interface LoadingAction extends Action<typeof LOADING> {
    page: number;
}

interface LoadedPageAction extends Action<typeof LOADED_PAGE> {
    notifications: Notification[];
    page: number;
    totalPages: number;
    itemsPerPage: number;
}

type ClearPagesAction = Action<typeof CLEAR_PAGES>;

interface MarkAsReadAction extends Action<typeof CHANGED_READ_STATUS> {
    notificationId: number;
    read: boolean;
}

interface DiscardedAction extends Action<typeof DISCARDED> {
    notificationId: number;
}

type UnsetLoadingAction = Action<typeof UNSET_LOADING>;

interface NewNotificationAction extends Action<typeof NEW_NOTIFICATION> {
    notification: Notification;
}

interface SimplyChangePage extends Action<typeof SIMPLY_CHANGE_PAGE> {
    page: number;
}

export type Actions = LoadedPageAction | ClearPagesAction | MarkAsReadAction | DiscardedAction | UnsetLoadingAction |
    NewNotificationAction | LoadingAction | SimplyChangePage;

export const loadPage = (data: Notification[][], page: number, unreadOnly: boolean = false) => f(async (dispatch: Dispatch) => {
    if(!data[page] || data[page].length === 0) {
        dispatch({ type: LOADING, page });
    }

    const notifications = await Notifications.getList(page, unreadOnly);
    dispatch({
        itemsPerPage: notifications.limit,
        notifications: notifications.results,
        page,
        totalPages: notifications.pages,
        type: LOADED_PAGE,
    });
});

export const clearPages = (): ClearPagesAction => ({
    type: CLEAR_PAGES,
});

export const markAsRead = (notification: Notification) => f(async (dispatch: Dispatch) => {
    const n = await Notifications.markAsRead(notification);
    dispatch({
        notificationId: n.id_notificacion,
        read: n.read,
        type: CHANGED_READ_STATUS,
    });
});

export const discard = (notification: Notification) => f(async (dispatch: Dispatch) => {
    await Notifications.discard(notification);
    dispatch({
        notificationId: notification.id_notificacion,
        read: !notification.read,
        type: DISCARDED,
    });
});

export const unsetLoading = () => ({
    type: UNSET_LOADING
});
