import { InjectedTranslateProps } from 'react-i18next';
import { RouteComponentProps } from 'react-router';

import { NotFoundError, Notification, ServerUnavailableError } from 'src/common/lib/api';
import { ErrorsState } from 'src/common/redux/errors/state';

export interface NotificationsPageStateToProps {
    loading: boolean;
    notifications?: Notification[];
    notFoundError: NotFoundError | null;
    serverError: ServerUnavailableError | null;
    totalPages?: number;
    coisas: Notification[][];
}

export interface NotificationsPageDispatchToProps {
    loadPage: (page: number, coisas: Notification[][]) => void;
    clearPages: () => void;
    markAsRead: (notification: Notification) => void;
    discard: (notification: Notification) => void;
    clearError: (what?: keyof ErrorsState) => void;
    clearLoading: () => void;
}

export interface NotificationsPageOwnProps extends InjectedTranslateProps, RouteComponentProps<{ page: string }> {}

export type NotificationsPageProps = NotificationsPageStateToProps & NotificationsPageDispatchToProps & NotificationsPageOwnProps;