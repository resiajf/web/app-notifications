import React from 'react';
import { translate } from 'react-i18next';
import { toast } from 'react-toastify';

import { EmailConfSection } from 'src/components/email-conf-section';
import { ConfigPageProps } from 'src/components/pages/config-page.interfaces';
import { TelegramConfSection } from 'src/components/telegram-conf-section';

class ConfigPageClass extends React.Component<ConfigPageProps> {

    private numeriyo: number | null = null;

    constructor(props: ConfigPageProps) {
        super(props);
    }

    public componentDidMount() {
        this.props.loadEmailConfig();
        this.props.loadTelegramConfig();
    }

    public componentDidUpdate(prevProps: Readonly<ConfigPageProps>) {
        if(!prevProps.serverError && this.props.serverError) {
            toast.error(<p>
                { this.props.t('server_has_failed') }<br/>
                <small>{ this.props.t(this.props.serverError.translatedMessageKey) }</small>
            </p>);
            if(this.props.updating) {
                this.props.clearUpdating();
            }
            if(this.props.telegramActivationTest.loading) {
                this.props.clearCheckingForTelegram();
            }
            setTimeout(() =>this.props.clearError('serverUnavailable'));
        }

        if(prevProps.telegramActivationTest.loading && !this.props.telegramActivationTest.loading && !this.props.serverError) {
            const { status } = this.props.telegramActivationTest;
            if(status) {
                if(status.done === false) {
                    toast.update(this.numeriyo!, {
                        autoClose: 3000,
                        render: (
                            <p>
                                { this.props.t('could_not_activate_telegram') }<br/>
                                <small>{ this.props.t('could_not_activate_telegram_' + status.reason) }</small>
                            </p>
                        ),
                        type: toast.TYPE.ERROR,
                    });
                    this.numeriyo = null;
                } else {
                    toast.update(this.numeriyo!, {
                        autoClose: 3000,
                        render: (
                            <p>
                                { this.props.t('checked_activation_success') }
                            </p>
                        ),
                        type: toast.TYPE.SUCCESS,
                    });
                }
            }
        }

        if(!prevProps.telegramActivationTest.loading && this.props.telegramActivationTest.loading) {
            this.numeriyo = toast(<p>{ this.props.t('checking_activation') }</p>, { autoClose: false });
        }

        if(prevProps.updating && !this.props.updating && !this.props.serverError) {
            toast.success(this.props.t('config.saved'));
        }
    }

    public render() {
        const { email, user, t, telegram, updating } = this.props;
        //TODO Que hacer en caso de que el usuario no sea de la Universidad

        return (
            <>
                <h1 className="display-4">{ t('config_title') }</h1>
                <EmailConfSection email={ email } t={ t } user={ user } updating={ updating }
                                  modifyEmailConfig={ this.props.changeEnabledStatusForEmail } />

                <hr/>

                <TelegramConfSection updating={ updating } t={ t } telegram={ telegram }
                                     modifyTelegramConfig={ this.props.changeEnabledStatusForTelegram }
                                     checkActivation={ this.props.checkActivationStatusForTelegram } />
            </>
        );
    }

}

export const ConfigPage = translate()(ConfigPageClass);