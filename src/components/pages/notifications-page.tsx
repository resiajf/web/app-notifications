import moment from 'moment';
import React from 'react';
import { translate } from 'react-i18next';
import { Redirect } from 'react-router';
import { toast } from 'react-toastify';

import { Button, Link } from 'src/common/components/bootstrap/buttons';
import LoadSpinner from 'src/common/components/load-spinner';
import Pagination from 'src/common/components/pagination';
import { Notification } from 'src/common/lib/api';
import { NotificationsPageProps } from 'src/components/pages/notifications-page.interfaces';

import emwtf from 'src/components/pages/emwtf.png';

interface NotificationCardProps {
    notification: Notification;
    onLinkClicked?: (link: string, notification: Notification) => void;
    onMarkAsReadClicked?: (notification: Notification) => void;
    onDiscardClicked?: (notification: Notification) => void;
    t: any;
}

const NotificationCard = ({ notification, onLinkClicked, onMarkAsReadClicked, onDiscardClicked, t }: NotificationCardProps) => {
    const click1 = (e: any) => {
        e.preventDefault();
        if(onLinkClicked) {
            onLinkClicked(notification.enlace!, notification);
        }
    };

    const click2 = (e: any) => {
        e.preventDefault();
        if(onMarkAsReadClicked) {
            onMarkAsReadClicked(notification);
        }
    };

    const click3 = (e: any) => {
        e.preventDefault();
        if(onDiscardClicked) {
            onDiscardClicked(notification);
        }
    };

    return (
        <div className={ `card mb-4 ${notification.read ? 'bg-light' : 'border-success'}` }>
            <div className="card-body">
                <h5 className="card-title">
                    { notification.titulo }
                    <small className="text-muted"> { moment.utc(notification.date_time).fromNow() }</small>
                </h5>
                <p className="card-text">
                    { notification.cuerpo }
                </p>
                { notification.enlace && <Link type="link" size="sm" className="card-link mr-1" href={ notification.enlace } onClick={ click1 }>{ t('go_link') }</Link> }
            </div>
            <div className="card-footer text-right">
                { !notification.read && <Button type="success" size="sm" outline={ true } className="mr-1" onClick={ click2 }>{ t('mark_as_read') }</Button> }
                <Button type="danger" size="sm" outline={ true } className="mr-1" onClick={ click3 }>{ t('discard') }</Button>
            </div>
        </div>
    );
};

class NotificationsPageClass extends React.Component<NotificationsPageProps> {

    constructor(props: NotificationsPageProps) {
        super(props);

        this.onMarkAsReadClicked = this.onMarkAsReadClicked.bind(this);
        this.onDiscardClicked = this.onDiscardClicked.bind(this);
        this.onLinkClicked = this.onLinkClicked.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
    }

    public componentDidMount() {
        if(!this.props.notifications) {
            this.props.loadPage(Number(this.props.match.params.page) || 0, this.props.coisas);
        }
    }

    public componentDidUpdate(prevProps: Readonly<NotificationsPageProps>) {
        if(prevProps.match.params.page !== this.props.match.params.page) {
            this.props.loadPage(Number(this.props.match.params.page), this.props.coisas);
        }

        if(!prevProps.notFoundError && this.props.notFoundError) {
            toast.warn(<p>{ this.props.notFoundError.translatedMessageKey }</p>);
            this.props.clearLoading();
            this.props.clearError('notFound');
        }

        if(prevProps.notifications && !this.props.notifications) {
            this.props.loadPage(Number(this.props.match.params.page) || 0, this.props.coisas);
        }

        if(!prevProps.serverError && this.props.serverError) {
            this.props.clearLoading();
            this.props.clearError('serverUnavailable');
        }
    }

    public componentWillUnmount() {
        this.props.clearPages();
    }

    public render() {
        const { totalPages, notifications, loading, t } = this.props;

        let content;
        if(loading) {
            content = <LoadSpinner />;
        } else if(notifications && notifications.length) {
            content = notifications.map((n, i) => <NotificationCard key={i}
                                                                    notification={ n }
                                                                    onLinkClicked={ this.onLinkClicked }
                                                                    onDiscardClicked={ this.onDiscardClicked }
                                                                    onMarkAsReadClicked={ this.onMarkAsReadClicked }
                                                                    t={t} />);
        } else if(this.props.match.params.page === '1') {
            content = <p className="lead text-muted text-center">{ t('no_notifications') }</p>;
        } else if(Number(this.props.match.params.page) === this.props.totalPages! + 1) {
            //If we are in the last page and we arrive here, is because we deleted the last notification of the last page
            content = <Redirect to={ `${this.props.match.path.substring(1, this.props.match.path.indexOf(':'))}/${this.props.totalPages!}` } />;
        } else {
            content = <div className="text-center">
                <img className="img-fluid" src={ emwtf } />
                <p className="lead">
                    What did you just done?
                </p>
            </div>;
        }

        return (
            <div>
                { totalPages !== undefined &&
                <Pagination page={ Number(this.props.match.params.page) - 1 }
                            pages={ totalPages  }
                            onChange={ this.onPageChange } /> }

                { content }

                { totalPages !== undefined &&
                <Pagination page={ Number(this.props.match.params.page) - 1 }
                            pages={ totalPages  }
                            onChange={ this.onPageChange } /> }
            </div>
        );
    }

    private onPageChange(page: number) {
        this.props.history.push(`${this.props.match.path.substring(0, this.props.match.path.indexOf(':'))}${page + 1}`);
    }

    private onMarkAsReadClicked(notification: Notification) {
        this.props.markAsRead(notification);
    }

    private onDiscardClicked(notification: Notification) {
        this.props.discard(notification);
    }

    private onLinkClicked(link: string, notification: Notification) {
        window.open(link, 'blank');
        this.props.markAsRead(notification);
        window.scrollTo({ top: 0 });
    }

}

export const NotificationsPage = translate()(NotificationsPageClass);