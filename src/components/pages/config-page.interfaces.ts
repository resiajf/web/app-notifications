import { InjectedTranslateProps } from 'react-i18next';
import { RouteComponentProps } from 'react-router';

import { ServerUnavailableError } from 'src/common/lib/api';
import { ErrorsState } from 'src/common/redux/errors/state';
import { User } from 'src/common/redux/user/state';
import { NotificationConfig } from 'src/redux/config/state';

export interface ConfigPageStateToProps extends NotificationConfig {
    serverError: ServerUnavailableError | null;
    user: User;
}

export interface ConfigPageDispatchToProps {
    loadEmailConfig: () => void;
    loadTelegramConfig: () => void;
    checkActivationStatusForTelegram: () => void;
    clearCheckingForTelegram: () => void;
    changeEnabledStatusForTelegram: (enabled: boolean, removeChatId?: boolean) => void;
    changeEnabledStatusForEmail: (enabled: boolean) => void;
    clearUpdating: () => void;
    clearError: (what?: keyof ErrorsState) => void;
}

export type ConfigPageOwnProps = InjectedTranslateProps & RouteComponentProps<{}>;

export type ConfigPageProps = ConfigPageStateToProps & ConfigPageDispatchToProps & ConfigPageOwnProps;