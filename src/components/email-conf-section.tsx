import React from 'react';
import { InjectedTranslateProps, Trans } from 'react-i18next';

import { NotificationConfigBase } from 'src/common/lib/api';
import { User } from 'src/common/redux/user/state';

interface EmailConfSectionProps extends InjectedTranslateProps {
    user: User;
    email?: NotificationConfigBase;
    updating: boolean;
    modifyEmailConfig: (enabled: boolean) => void;
}

export class EmailConfSection extends React.Component<EmailConfSectionProps> {

    constructor(props: EmailConfSectionProps) {
        super(props);

        this.onInputChange = this.onInputChange.bind(this);
    }

    public render() {
        const { email, t, updating, user } = this.props;

        return (
            <>
                <h2>{ t('config.email') }</h2>
                <p className="lead">
                    <Trans i18nKey="config.email_description">
                        . {{ email: `${user.niu}@uma.es` }} .
                    </Trans>
                </p>
                <div className="form-check">
                    <input type="checkbox"
                           id="email-enabler"
                           className="form-check-input"
                           disabled={ email === undefined || updating }
                           checked={ email ? email.enabled : false }
                           onChange={ this.onInputChange } />
                    <label htmlFor="email-enabler" className="form-check-label">
                        { t('config.email_enabled') }
                    </label>
                </div>
            </>
        );
    }

    private onInputChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.props.modifyEmailConfig(e.target.checked);
    }

}