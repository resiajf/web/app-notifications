import React from 'react';
import { InjectedTranslateProps, Trans } from 'react-i18next';

import { Button } from 'src/common/components/bootstrap/buttons';
import { NotificationConfigTelegram } from 'src/common/lib/api';

interface TelegramConfSectionProps extends InjectedTranslateProps {
    telegram?: NotificationConfigTelegram;
    updating: boolean;
    modifyTelegramConfig: (enabled: boolean, forgetti?: boolean) => void;
    checkActivation: () => void;
}

export class TelegramConfSection extends React.Component<TelegramConfSectionProps> {

    constructor(props: TelegramConfSectionProps) {
        super(props);

        this.checkActivation = this.checkActivation.bind(this);
        this.onInputChange = this.onInputChange.bind(this);
        this.forgetData = this.forgetData.bind(this);
    }

    public render() {
        const { telegram, t, updating } = this.props;

        const botName = 'noti_resi_afj';

        return (
            <>
                <h2>{ t('config.telegram') }</h2>
                <p className="lead">
                    <Trans i18nKey="config.telegram_description">
                        . <a href={ `https://telegram.me/${botName}` } target="_blank" rel="noreferrer">{{ botName }}</a> .
                    </Trans>
                </p>
                <div className="form-check">
                    <input type="checkbox"
                           id="telegram-enabler"
                           className="form-check-input"
                           disabled={ telegram === undefined || updating || telegram.chat_id === null }
                           checked={ telegram ? telegram.enabled : false }
                           onChange={ this.onInputChange } />
                    <label htmlFor="telegram-enabler" className="form-check-label">{ t('config.telegram_enabled') }</label>
                </div>
                { telegram && telegram.chat_id === null ? <div className="mt-4">
                    <span>
                        <Trans i18nKey="config.telegram_activation_code">
                            Here's your code hehe <code>{{ code: (telegram as any).activation_code }}</code> xD
                        </Trans>
                    </span>
                    <Button type="primary" outline={ true } size="sm" className="ml-2" onClick={ this.checkActivation }>
                        { t('config.telegram_activation_button') }
                    </Button>
                    <p className="text-muted"><small>{ t('config.telegram_activation_help') }</small></p>
                </div> : false }
                { telegram && telegram.chat_id !== null && <div className="mt-2 ml-4">
                    <Button type="secondary" outline={ true } size="sm" onClick={ this.forgetData }>
                        { t('config.telegram_forget_user') }
                    </Button>
                    <p className="text-muted"><small>{ t('config.telegram_forget_user_help') }</small></p>
                </div> }
            </>
        );
    }

    private onInputChange(e: React.ChangeEvent<HTMLInputElement>) {
        this.props.modifyTelegramConfig(e.target.checked);
    }

    private checkActivation(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.props.checkActivation();
    }

    private forgetData(e: React.MouseEvent<HTMLButtonElement>) {
        e.preventDefault();
        this.props.modifyTelegramConfig(false, true);
    }

}